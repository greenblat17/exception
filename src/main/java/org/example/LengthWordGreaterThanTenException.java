package org.example;

public class LengthWordGreaterThanTenException extends Exception {


    public LengthWordGreaterThanTenException(String message) {
        super(message);
    }
}
