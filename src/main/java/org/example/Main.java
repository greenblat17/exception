package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            var s = wordException();
            System.out.println(s);
        } catch (LengthWordGreaterThanTenException e) {
            System.out.println(e.getMessage());
        }
    }

    private static String wordException() throws LengthWordGreaterThanTenException {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.next();
        if (word.length() > 10)
            throw new LengthWordGreaterThanTenException("Word length is greater than 10. Length word '" + word + "' is: " + word.length());

        return word;
    }
}